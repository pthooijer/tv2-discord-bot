import discord
import configparser
import re
import random

import tv2_bot_basic
import tv2_bot_help
import tv2_bot_time

config = configparser.ConfigParser()
config.read('config.ini')

cred_config = configparser.ConfigParser()
cred_config.read('credentials.cred')

client = discord.Client()
discord_token = cred_config['Discord']['token']

random_pattern = re.compile('^!random (\d)+-(\d+)$')

async def reply(original_message, reply):
	await client.send_message(original_message.channel, reply)

@client.event
async def on_ready():
	print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
	if message.author == client.user:
		return

	content = message.content.strip()
		
	if content.startswith('!hello'):
		await reply(message, tv2_bot_basic.hello_message(message.author))
		
	if content.startswith('!random'):
		match = random_pattern.match(content)
		if match:
			a = int(match.group(1))
			b = int(match.group(2))
			if a > b:
				await reply(message, tv2_bot_help.usage(content))
			else:			
				await reply(message, '{0}'.format(random.randint(a, b)))
		else:
			await reply(message, tv2_bot_help.usage(content))
		
	if content.startswith('!lpt-time'):
		await reply(message, tv2_bot_time.lpt_time_message())
			
client.run(discord_token)