
usage_commands = {
	'!hello' : '"!hello".',
	'!lpt-contest': '"!lpt-contest".',
	'!random': '"!random a-b", where a and b are non-negative integers and a ≤ b.'
}

help_commands = {
	'!hello' : 'Returns "Hello @user!"',
	'!lpt-contest': 'Returns the time until the next lets-play-together contest, or if already in a contest, the time until the contest finishes.',
	'!random': 'Returns a random integer between a and b (inclusive).'
}

def usage(string):
	first_word = string.split()[0]
	if first_word in usage_commands:
		return 'Usage: {0}'.format(usage_commands.get(first_word))
	else:
		return '"{0}" is an unknown command.'.format(first_word)
		
def help(string):
	first_word = string.split()[0]
	if first_word in help_commands:
		return '{0}: {1}'.format(first_word, help_commands.get(first_word))
	else:
		return '"{0}" is an unknown command.'.format(first_word)