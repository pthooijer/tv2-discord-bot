from datetime import datetime
from datetime import timezone
from datetime import time
from datetime import date
from datetime import timedelta
from dateutil import tz
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

def parse_time(timestr, timezone):
	h, m, s = map(int, timestr.split(':'))
	return time(h, m, s, tzinfo=timezone)

used_timezone = tz.gettz(config['Time']['timezone'])
lpt_start_weekday = int(config['Time']['lpt_start_weekday'])
lpt_start_time = parse_time(config['Time']['lpt_start_time'], used_timezone)
lpt_end_weekday = int(config['Time']['lpt_end_weekday'])
lpt_end_time = parse_time(config['Time']['lpt_end_time'], used_timezone)

def next_date_with_weekday(startdate, isoweekday):
	for i in range(0, 7):
		test_date = startdate + timedelta(days=i)
		if test_date.isoweekday() == isoweekday:
			return test_date

def format_timedelta(time_delta):
	rounded_timedelta = timedelta(seconds=round(time_delta.total_seconds()))
	return str(rounded_timedelta)

def lpt_time_message():
	datetime_now = datetime.now(used_timezone)

	lpt_start_date = next_date_with_weekday(datetime_now.date(), lpt_start_weekday) # Can be current date
	if datetime_now.isoweekday() == lpt_start_weekday and datetime_now.time() > lpt_start_time:
		lpt_start_date = lpt_start_date + timedelta(days=7)
	lpt_start_datetime = datetime.combine(lpt_start_date, lpt_start_time, used_timezone)

	lpt_end_date = next_date_with_weekday(datetime_now.date(), lpt_end_weekday) # Can be current date
	if datetime_now.isoweekday() == lpt_end_weekday and datetime_now.time() > lpt_end_time:
		lpt_end_date = lpt_end_date + timedelta(days=7)
	lpt_end_datetime = datetime.combine(lpt_end_date, lpt_end_time, used_timezone)

	if lpt_start_datetime < lpt_end_datetime:
		timediff = lpt_start_datetime - datetime_now
		return 'Contest starts in ' + format_timedelta(timediff)
	else:
		timediff = lpt_end_datetime - datetime_now
		return 'Contest ends in ' + format_timedelta(timediff)
			